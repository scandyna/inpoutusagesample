// SPDX-License-Identifier: MIT
#include "InpOutDll.h"
#include <iostream>

int main()
{
  std::cout << "Initializing InpOut ..." << std::endl;

  InpOutDll io;

  short address = 0x61;
  std::cout << "Initializing InpOut DONE\nReading at some random address '" << address << "' (I dont't know Windows IOCTL) ..." << std::endl;
  std::cout << "->: " << io.read(address) << std::endl;

  std::cout << "Just a sandbox - Have a nice day :)" << std::endl;
}
