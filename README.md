
# Introduction

Simple example to use `InpOut` by loading its compiled dll.

See https://highrez.co.uk/

# Build

Example using MinGW make, Debug build.

```Shell
mkdir $SomePathToABuildDir/Debug
cd $SomePathToABuildDir/Debug
cmake -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug $PathToSource
cmake-gui .

mingw32-make
```

# Notes

To keep things simple, we have a compiled version of inpout dll in the repository.

At the first run, the driver has to be installed
as described here: https://highrez.co.uk/ .

# Disclaimer

This will install a driver that gives low level controls in user space.
This can lead to security issues.
Use it at your own risk.

# Rationale

The first attempt was to compile the InpOut driver and driver interface.
This requires the [ntddk.h](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/ntddk/) header.
I think we land in the driver development space here.
This probably requires the appropriate SDK and also MSVC
(I don't know if we can compile a driver with Gcc for Windows, probably not).

The second attempt was the way recommended by [highrez](https://highrez.co.uk/):
Load the dll at runtime.
Thanks to the fact that the driver interface only exports C symbols (not C++ ones),
we can compile our code with Gcc 13, and load a dll that was compiled with MSVC over 10 years ago.
