// SPDX-License-Identifier: MIT
#include "InpOutDll.h"
#include <libloaderapi.h>
#include <errhandlingapi.h>
#include <string>
#include <cassert>

class InpOutDllImpl
{
  typedef void(__stdcall *WriteFunction)(short, short);
  typedef short(__stdcall *ReadFunction)(short);
  typedef BOOL(__stdcall *IsInpOutDriverOpenFunction)(void);

 public:

  InpOutDllImpl()
  {
    mInpOutDll = LoadLibraryA("inpoutx64.dll");
    if(mInpOutDll == NULL){
      const auto errorCode = GetLastError();
      const std::string msg = "Loading inpoutx64.dll failed with code " + std::to_string(errorCode);
      throw InpOutError(msg);
    }

    mWrite = reinterpret_cast<WriteFunction>( GetProcAddress(mInpOutDll, "Out32") );
    if(mWrite == nullptr){
      const auto errorCode = GetLastError();
      const std::string msg = "Could not find the Out32 function in inpoutx64.dll. Error code " + std::to_string(errorCode);
      throw InpOutError(msg);
    }

    mRead = reinterpret_cast<ReadFunction>( GetProcAddress(mInpOutDll, "Inp32") );
    if(mRead == nullptr){
      const auto errorCode = GetLastError();
      const std::string msg = "Could not find the Inp32 function in inpoutx64.dll. Error code " + std::to_string(errorCode);
      throw InpOutError(msg);
    }

    mIsInpOutDriverOpen = reinterpret_cast<IsInpOutDriverOpenFunction>( GetProcAddress(mInpOutDll, "IsInpOutDriverOpen") );
    if(mIsInpOutDriverOpen == nullptr){
      const auto errorCode = GetLastError();
      const std::string msg = "Could not find the IsInpOutDriverOpen function in inpoutx64.dll. Error code " + std::to_string(errorCode);
      throw InpOutError(msg);
    }

    if( !isOpen() ){
      const std::string msg = "Could not open the InpOut driver."
      "If you run an executable with InpOut for the first time, you have to run it in elevated mode (Administrator rights) "
      "so that the driver can be installed.";
      throw InpOutError(msg);
    }
  }

  ~InpOutDllImpl() noexcept
  {
    FreeLibrary(mInpOutDll);
  }

  bool isOpen()
  {
    assert(mInpOutDll != NULL);
    assert(mIsInpOutDriverOpen != nullptr);

    return mIsInpOutDriverOpen();
  }

  void write(short portAddress, short data)
  {
    assert(mInpOutDll != NULL);
    assert(mWrite != nullptr);

    mWrite(portAddress, data);
  }

  short read(short portAddress)
  {
    assert(mInpOutDll != NULL);
    assert(mRead != nullptr);

    return mRead(portAddress);
  }

 private:

  HMODULE mInpOutDll;
  WriteFunction mWrite = nullptr;
  ReadFunction mRead = nullptr;
  IsInpOutDriverOpenFunction mIsInpOutDriverOpen = nullptr;
};


InpOutDll::InpOutDll()
 : InpOut(),
   mImpl( std::make_unique<InpOutDllImpl>() )
{
}

InpOutDll::~InpOutDll() noexcept = default;

void InpOutDll::doWrite(short portAddress, short data)
{
  mImpl->write(portAddress, data);
}

short InpOutDll::doRead(short portAddress)
{
  return mImpl->read(portAddress);
}
