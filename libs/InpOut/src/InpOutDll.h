// SPDX-License-Identifier: MIT
#pragma once

#include "InpOut.h"
#include "inpout_export.h"
#include <memory>

/*! \internal
 *
 * We don't want to leak Windows headers out from the implementation.
 */
class InpOutDllImpl;

/*! \brief Implementation of InpOut that works loading io.dll
 */
class INPOUT_EXPORT InpOutDll : public InpOut
{
 public:

  /*! \brief Construct an instance
   */
  InpOutDll();

  ~InpOutDll() noexcept;

 private:

  void doWrite(short portAddress, short data) override;
  short doRead(short portAddress) override;

  std::unique_ptr<InpOutDllImpl> mImpl;
};
