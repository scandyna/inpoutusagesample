// SPDX-License-Identifier: MIT
#pragma once

#include "InpOutError.h"
#include "inpout_export.h"

/*! \brief Wrapper to InpOut
 */
class INPOUT_EXPORT InpOut
{
 public:

  /*! \brief Constructor
   *
   * \exception  InpOutError
   */
  InpOut() = default;

  virtual
  ~InpOut() noexcept = default;

  // Disable copy and move
  InpOut(const InpOut &) = delete;
  InpOut & operator=(const InpOut &) = delete;
  InpOut(InpOut &&) = delete;
  InpOut & operator=(InpOut &&) = delete;

  /*! \brief Write given data to given port address
   */
  void write(short portAddress, short data)
  {
    doWrite(portAddress, data);
  }

  /*! \brief Read data from given port address
   */
  short read(short portAddress)
  {
    return doRead(portAddress);
  }

 private:

  virtual
  void doWrite(short portAddress, short data) = 0;

  virtual
  short doRead(short portAddress) = 0;
};
