// SPDX-License-Identifier: MIT
#pragma once

#include <stdexcept>
#include <string>

/*! \brief InpOut error
 */
class InpOutError : public std::runtime_error
{
 public:

  InpOutError() = delete;

  explicit
  InpOutError(const std::string & msg)
   : runtime_error(msg)
  {
  }
};
